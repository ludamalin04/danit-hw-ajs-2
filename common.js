const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const root = document.querySelector('#root');
const list = document.createElement('ul');
root.append(list);

const parameters = ['author', 'name', 'price'];

books.forEach(function (item, index) {
    if (item.author && item.name && item.price) {
        const book = document.createElement('li');
        list.append(book);
        book.textContent = `${item.author} "${item.name}", ${item.price} грн`;
    } else {
        try {
            for (let value of parameters) {
                if (!item.hasOwnProperty(value)) {
                    throw new Error(`Error: book ${index + 1} has no ${value}`)
                }
            }
        } catch (error) {
            console.log(error.message);
        }
    }
})






 





